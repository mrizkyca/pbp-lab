import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Artikel Covid-19',
      theme: ThemeData(
        brightness: Brightness.dark,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  final List gambar = [
    "https://akcdn.detik.net.id/visual/2021/07/07/infografisingin-mendapatkan-bansos-tunai-rp-600000-begini-caranyaaristya-rahadian_169.png?w=715&q=90",
    "https://akcdn.detik.net.id/community/media/visual/2020/03/23/3747d382-8338-413d-bae1-56c29d0aae6c_169.jpeg?w=700&q=90",
    "https://farmalkes.kemkes.go.id/wp-content/uploads/2021/01/vaksinasi-1024x576.jpg"
  ];

  final List judul = [
    "Korupsi Dana Bansos",
    "Jokowi di Kala Pandemi",
    "Vaksinasi Nasional Dipercepat"
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
        title: Text("Galeri Artikel Covid-19"),
      ),
      body: Center(
        child: Container(
          padding: const EdgeInsets.only(left: 14, right: 14),
          child: ListView.builder(
              itemCount: gambar.length,
              itemBuilder: (context, index) {
                return Card(
                  elevation: 14,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(14)),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Stack(
                        alignment: Alignment.bottomLeft,
                        children: [
                          Image.network(
                            gambar[index],
                            fit: BoxFit.cover,
                          ),
                        ],
                      ),
                      Padding(
                        padding:
                            const EdgeInsets.only(left: 16, top: 16, right: 16),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(judul[index],
                                style: new TextStyle(
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.bold)),
                            SizedBox(
                              height: 10.0,
                            ),
                            Text(
                                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus lobortis tempus fringilla. Cras mattis, enim nec iaculis bibendum, ante neque pulvinar nisi, sed imperdiet ligula.")
                          ],
                        ),
                      ),
                      ButtonBar(
                        children: [
                          OutlinedButton(
                            onPressed: () {},
                            child: Text("Read More"),
                          ),
                        ],
                      )
                    ],
                  ),
                );
              }),
        ),
      ),
    );
  }
}
