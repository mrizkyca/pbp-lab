from django.shortcuts import redirect, render
from lab_2.models import Note
from .forms import NoteForm


def index(request):
    notes = Note.objects.all().values()
    return render(request, 'lab4_index.html', {'notes': notes})


def add_note(request):
    form = NoteForm(request.POST)
    if (form.is_valid and request.method == "POST"):
        form.save()
        return redirect('/lab-4')
    return render(request, 'lab4_form.html', {'form': form})


def note_list(request):
    notes = Note.objects.all().values()
    return render(request, 'lab4_note_list.html', {'notes': notes})
