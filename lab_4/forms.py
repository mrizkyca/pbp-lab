from django import forms
from lab_2.models import Note


class NoteForm(forms.ModelForm):
    class Meta():
        model = Note
        fields = ['ke', 'dari', 'judul', 'pesan']
        widgets = {
            'ke': forms.TextInput(attrs={'class': 'form-control'}),
            'dari': forms.TextInput(attrs={'class': 'form-control'}),
            'judul': forms.TextInput(attrs={'class': 'form-control'}),
            'pesan': forms.TextInput(attrs={'class': 'form-control'})
        }
