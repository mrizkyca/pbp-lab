import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Form Artikel Covid-19',
      theme: ThemeData(primarySwatch: Colors.blue, brightness: Brightness.dark),
      home: MyCustomPage(),
    );
  }
}

class MyCustomPage extends StatefulWidget {
  const MyCustomPage({Key? key}) : super(key: key);
  @override
  MyHomePage createState() {
    return MyHomePage();
  }
}

class MyHomePage extends State<MyCustomPage> {
  final judulArtikel = TextEditingController();
  final isiArtikel = TextEditingController();
  bool _validateJudul = false;
  bool _validateIsi = false;

  @override
  Widget build(BuildContext context) {
    void sendData() {
      AlertDialog alertDialog = new AlertDialog(
          content: new Container(
              height: 105.0,
              child: new Column(children: <Widget>[
                new Text(
                    "Artikel dengan judul '${judulArtikel.text}' berhasil ditambahkan!"),
                new Padding(padding: new EdgeInsets.only(top: 20.0)),
                new ElevatedButton(
                    onPressed: () => Navigator.of(context).pop(),
                    child: new Text("OK"))
              ])));
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return alertDialog;
          });
    }

    void clearField() {
      judulArtikel.clear();
      isiArtikel.clear();
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
        title: Text("Form Artikel Covid-19"),
      ),
      body: Center(
        child: Container(
          padding: const EdgeInsets.only(left: 14, right: 14),
          child: new Column(
            children: <Widget>[
              new Padding(padding: new EdgeInsets.only(top: 25.0)),
              new TextField(
                controller: judulArtikel,
                maxLength: 50,
                decoration: new InputDecoration(
                    hintText: "Korupsi Bansos di Kala Pandemi",
                    errorText:
                        _validateJudul ? 'Harap masukkan judul artikel' : null,
                    labelText: "Judul Artikel",
                    border: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(20.0))),
              ),
              new Padding(padding: new EdgeInsets.only(top: 15.0)),
              new TextField(
                controller: isiArtikel,
                maxLines: 3,
                decoration: new InputDecoration(
                    errorText:
                        _validateIsi ? 'Harap masukkan isi artikel' : null,
                    hintText:
                        "Mantan penyidik senior KPK (Komisi Pemberantasa Korupsi), Novel Baswedan mengungkapkan penanganan korupsi Bansos atau Bantuan Sosial sebagai penanganan yang dicitrakan. Hal ini ia ungkapkan dalam akun Youtube Novel Baswedan, dengan judul Otak Dibalik Kasus Bansos Belum Terungkap!, pada 6 November 2021.",
                    labelText: "Isi Artikel",
                    border: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(20.0))),
              ),
              new Padding(padding: new EdgeInsets.only(top: 20.0)),
              new ElevatedButton(
                  onPressed: () {
                    setState(() {
                      judulArtikel.text.isEmpty
                          ? _validateJudul = true
                          : _validateJudul = false;
                      isiArtikel.text.isEmpty
                          ? _validateIsi = true
                          : _validateIsi = false;
                    });
                    if (_validateJudul == false && _validateIsi == false) {
                      sendData();
                      clearField();
                    }
                  },
                  child: new Text("Save Artikel"))
            ],
          ),
        ),
      ),
    );
  }
}
