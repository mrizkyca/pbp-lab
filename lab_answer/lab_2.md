1. Apakah perbedaan antara JSON dan XML?

JavaScript object notation atau JSON adalah salah satu format yang digunakan untuk menyimpan dan mentransfer data dalam aplikasi web. JSON sering digunakan pada API karena memiliki struktur data yang sederhana dan mudah dipahami. Struktur utama pada JSON adalah kumpulan value berpasangan (Object) dan daftar value berurutan (Array). Kode JSON lebih ringkas karena tidak memerlukan tag pembuka dan penutup.

XML adalah singkatan dari "eXtensible Markup Language“. Sintaks XML berbasis tag yang sangat mirip dengan HTML. Keuntungan utama XML adalah memungkinkan pemrogram membuat tag sendiri. XML dapat dibaca di sisi klien maupun di sisi server. Selain itu, XML juga dapat digunakan sebagai jembatan antara dua sistem yang berbeda. Namun, saat data menjadi besar dan kompleks, mungkin sulit untuk membaca XML.

Beberapa perbedaan antara JSON dengan XML yaitu:

- JSON adalah bahasa meta sedangkan XML adalah bahasa markup
- JSON sederhana dan mudah dibaca sedangkan XML lebih rumit
- JSON mendukung array sedangkan XML tidak mendukung array
- File JSON berekstensi .json sedangkan File XML berekstensi .xml

2. Apakah perbedaan antara HTML dan XML?

Hypertext Markup Language atau HTML adalah bahasa markup yang berfungsi untuk membuat struktur website sehingga dapat diakses secara umum melalui aplikasi browser. Komponen-komponen yang terdapat pada HTML terdiri dari tag, elemen, dan atribut yang akan menyusun bagian heading, paragraf, isi konten, dan lainnya. Dokumen HTML yang dibuat memiliki format ekstensi .html. Ekstensi file ini akan dibaca dan di-render menjadi tampilan halaman website oleh web browser.

Beberapa perbedaan antara HTML dengan XML yaitu:

- HTML untuk menampilkan data sedangkan XML untuk menyimpan dan mengirimkan data
- HTML menggunakan tag-tag tertentu untuk menjalankan fungsinya sedangkan XML bebas menggunakan tag yang dibuat sendiri
- HTML case insensitive sedangkan XML case sensitive
- HTML static typing sedangkan XML dynamic typing
